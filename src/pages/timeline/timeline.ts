import { Component } from '@angular/core';
import { App, NavController, NavParams, ModalController } from 'ionic-angular';
import { ModalPage } from '../modal/modal';
import { AddgigPage } from '../addgig/addgig';


@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html'
})
export class TimelinePage {

  constructor(public app: App, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimelinePage');
  }

  showModal(src){
      let profileModal = this.modalCtrl.create(ModalPage, { photo: src });
      profileModal.present();
  }

  addgig(){
      this.app.getRootNav().push(AddgigPage); //take to the gig page to add gig
  }

}
