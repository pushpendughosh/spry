import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactsPage } from '../contacts/contacts';
import { TimelinePage } from '../timeline/timeline';
import { ChatsPage } from '../chats/chats';
import { MygigsPage } from '../mygigs/mygigs';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  contacts:any;
  timeline:any;
  chats:any;
  mygigs:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.contacts = ContactsPage;
    this.timeline = TimelinePage;
    this.chats = ChatsPage;
    this.mygigs = MygigsPage;     
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

}
