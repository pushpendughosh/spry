import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { Storage } from '@ionic/storage';
import {AngularFire, FirebaseListObservable} from 'angularfire2';


@Component({
  selector: 'page-addgig',
  templateUrl: 'addgig.html',
  providers:[System]
})
export class AddgigPage {

  image1:string = "assets/img/noimage.jpg";
  image2:string = "assets/img/noimage.jpg";
  image3:string = "assets/img/noimage.jpg";
  title:string = "";
  description:string = "";
  price:string;
  mobile:string = "9734072595";

  constructor(public af: AngularFire, public system: System, public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {

       let base = this;
      //  base.storage.ready().then(() => {
      //      base.storage.get('currentuser').then((val) => {
      //          if(val != undefined){
      //                 let data = JSON.parse(val);
      //                 this.mobile = data.number;
      //          }
      //       });
      //  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddgigPage');
  }

  createGig(){
    let base = this;
    if(base.title.length > 50 || base.title.trim().length == 0){
       base.system.showLoader("title can not be empty or can not exceed more than 50 characters",2000,false);
       return;
    }

    if(base.description.length > 300 || base.description.trim().length == 0){
       base.system.showLoader("description can not be empty or can not exceed more than 300 characters",2000,false);
       return;
    }

    if(base.price == undefined){
       base.system.showLoader("price can not be empty",2000,false);
       return;
    }

      let d = new Date();
      let time = d.getTime();

    let gData = {
       title:base.title,
       price:base.price,
       description:base.description,
       image1:base.image1,
       image2:base.image2,
       image3:base.image3,
       views:0,
       number:base.mobile,
       time:time
    };

    let ref = base.af.database.list('/gigs/'+base.mobile);
    ref.push(gData).then(function(){
         base.navCtrl.pop();
    });

  }

}
