import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {System} from '../../providers/system';
import {DashboardPage} from '../dashboard/dashboard';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';



@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
  providers:[System]
})
export class SetupPage {

  name:string = "";
  profession:string = "";
  items: FirebaseListObservable<any[]>;
  avatar:string = "assets/img/avatar.png";
  mobile:string = "";

  /* camera options */
  options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth:400,
        targetHeight:400
  }

  constructor(public contacts: Contacts, public sqlite: SQLite, public camera: Camera, public af: AngularFire, public navCtrl: NavController, public navParams: NavParams, public system: System,public loadingCtrl: LoadingController, public params: NavParams,public storage: Storage) {
     this.mobile = this.params.get('mobile');
     this.storage.set('contactlist',"");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetupPage');
  }

  uploadimage(){
      let base = this;
        this.camera.getPicture(base.options).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        base.avatar = base64Image;
        }, (err) => {
        });
  }

  completeregistration(){
      let base = this;
      if(base.name == ""){
          base.system.showLoader("Display name can not be empty",2000,false);
      }else if(base.profession == ""){
          base.system.showLoader("You must fill your profession",2000,false);
      }else{
                         //false loader
                         let loading = base.loadingCtrl.create({
                            content: 'syncing server data',
                            duration: 10000
                          });
                          loading.onDidDismiss(() => {
                             base.navCtrl.setRoot(DashboardPage);
                          });
                          loading.present();
                          //false loader

        let ref = base.af.database.object('/users/'+base.mobile);
        base.storage.set('mobile',base.mobile); //localstorage
        ref.subscribe(
            val => {
               let hasuser = val.$exists();
               if(hasuser){
                  let data = {
                      name:base.name,
                      profession:base.profession,
                      avatar:base.avatar
                   }; // data to be updated
                   ref.update(data);
               }else{
                   let data = {
                       name : base.name,
                       avatar : base.avatar,
                       status : "Hey ! i am using spry",
                       profession : base.profession,
                       lastupdate : "0",
                       location : "0",
                   }; // data to be pushed
                   ref.set(data); //push to database
               }

                   let data = {
                       name : base.name,
                       avatar : base.avatar,
                       status : "Hey ! i am using spry",
                       profession : base.profession,
                       lastupdate : "0",
                       location : "0",
                       number: base.mobile
                   }; // data to be pushed
                   this.storage.set('currentuser',JSON.stringify(data));

                    this.sqlite.create({
                    name: 'localspry.db',
                    location: 'default'
                    })
                    .then((db: SQLiteObject) => {

                        /** create table for chat messages **/
                        let chatquery = "create table if not exists chats(id INTEGER PRIMARY KEY AUTOINCREMENT,key varchar(50),number VARCHAR(20),sender VARCHAR(15),receiver VARCHAR(15),body VARCHAR(30),type VARCHAR(10),time VARCHAR(25),status VARCHAR(10))";
                        db.executeSql(chatquery, {})
                        .then(() =>{
                                  //contacts
                                  console.log();
                        })
                        .catch(e => console.log(e));

                        let query = "create table if not exists contacts(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(32),number VARCHAR(20), profession VARCHAR(30),status VARCHAR(200), avatar VARCHAR(9999),petname VARCHAR(32),lastupdate VARCHAR(20),location VARCHAR(20),display VARCHAR(10),CONSTRAINT name_unique UNIQUE (number))";
                        db.executeSql(query, {})
                        .then(() =>{
                                  //contacts
                                let options = {   
                                filter : "",                                
                                multiple: true,        
                                hasPhoneNumber:true,                             
                                fields:  [ 'displayName', 'name' ]
                                };

                                base.contacts.find([ 'displayName', 'name' ],options).then((contacts) => {

                                      //on contact success
                                           let base = this;
                                                for(var a=0;a<=contacts.length-1;a++){
                                                    let number = contacts[a].phoneNumbers[0].value.replace('-','').replace(')','').replace('(','').replace('+','').replace(' ','').replace('  ','').trim().slice(-10);
                                                    let name = contacts[a].displayName;
                                                    let avatar = "assets/img/avatar.png";
                                                    let status = "Hey ! i am using spry";
                                                    let petname = "pet name";
                                                    let profession = "spry";
                                                    let lastupdate = "0";
                                                    let location = "0";
                                                    let display = "none";
                                                    //start statement
                                                    if( number != base.mobile ){
                                                            let query = "INSERT INTO contacts (name,number,avatar,profession,petname,status,lastupdate,location,display) VALUES ('"+name+"','"+number+"','"+avatar+"','"+profession+"','"+petname+"','"+status+"','"+lastupdate+"','"+location+"','"+display+"')";
                                                            db.executeSql(query, {}).then(success=>{
                                                                console.log(success);
                                                            },error=>{
                                                                console.log(error);
                                                            });
                                                    }
                                                    //end statement
                                                }

                                }, (error) => {
                                console.log(error);
                                })
                                //end contacts
                        })
                        .catch(e => console.log(e));

                    })
                    .catch(e => console.log(e));

            }
        );
      }
  }

}
