import { Component } from '@angular/core';
import { App, NavController, NavParams, ModalController } from 'ionic-angular';
import { System } from '../../providers/system';
import { Storage } from '@ionic/storage';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { AddgigPage } from '../addgig/addgig';
import { ModalPage } from '../modal/modal';


@Component({
  selector: 'page-mygigs',
  templateUrl: 'mygigs.html'
})
export class MygigsPage {

    mobile:string = "9734072595";
    gigs:any = [];

  constructor(public modalCtrl: ModalController, public app: App, public af: AngularFire, public system: System, public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {

       let base = this;
      //  base.storage.ready().then(() => {
      //      base.storage.get('currentuser').then((val) => {
      //          if(val != undefined){
      //                 let data = JSON.parse(val);
      //                 this.mobile = data.number;
      //          }
      //       });
      //  });
      base.subscribeFirebase();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MygigsPage');
  }

  addgig(){
      this.app.getRootNav().push(AddgigPage); //take to the gig page to add gig
  }

  subscribeFirebase(){
    let base = this;
    let ref = base.af.database.list('/gigs/'+base.mobile);
    ref.subscribe(val => {
        console.log(val);
        base.gigs = [];
        for(var i=0;i<=val.length-1;i++){
             let gData = val[i];
             let title = gData.title;
             let price = gData.price;
             let description = gData.description;
             let image1 = gData.image1; 
             let image2 = gData.image2;
             let image3 = gData.image3;
             let number = gData.number;
             let views = gData.views;
             let time = base.compareTime(gData.time);
             let pData = {
               title:title,
               price:price,
               description:description,
               image1:image1,
               image2:image2,
               image3:image3,
               number:number,
               views:views,
               time:time 
             };
             base.gigs.push(pData);
        }
    });
  }

 showModal(src){
      let profileModal = this.modalCtrl.create(ModalPage, { photo: src });
      profileModal.present();
 }

 compareTime(time){
      let d = new Date();
      let currentTime = d.getTime().toString();
      var currentDiff = parseInt(currentTime)-parseInt(time);
      var secDiff = currentDiff/1000;
      if(secDiff <= 60 ){
          return 'few sec ago';
      }else if(secDiff <= 1800){
        return 'rew min ago';
      }else if(secDiff > 1800 && secDiff < 3600){
        return 'half hour ago';
      }else if(secDiff >3600 && secDiff < 24*3600){
          return (secDiff/3600).toString().split('.')[0]+'hours ago';
      }else{
      
      var u = new Date(time);
      return u.getUTCFullYear() +
        '/' + ('0' + u.getUTCMonth()).slice(-2) +
        '/' + ('0' + u.getUTCDate()).slice(-2) + 
        ' , ' + ('0' + u.getUTCHours()).slice(-2) +
        ':' + ('0' + u.getUTCMinutes()).slice(-2);
      }
   }

}
