import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html'
})
export class ModalPage {

  photo:string = "";
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.photo = this.navParams.get('photo');
    console.log(this.photo);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

}
