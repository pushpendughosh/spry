import { Component } from '@angular/core';
import { App,NavController, NavParams, ViewController } from 'ionic-angular';
import { MyprofilePage } from '../myprofile/myprofile';

@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {

  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  myprofile(){
    this.close();
    this.app.getRootNav().push(MyprofilePage);
  }

}
