import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { SetupPage } from '../setup/setup';
import { Request } from '../../providers/request';


@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
    providers:[System,Request]
})
export class OtpPage {
  code:string = "";
  realcode:any = "";
  mobile:string = "";
  teleCode:string = "";
  constructor(public navCtrl: NavController, public params: NavParams, public system: System,public request: Request) {
    this.realcode = this.params.get('otp');
    this.mobile = this.params.get('mobile');
    this.teleCode = this.params.get('teleCode');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  sendcode(){
     let base = this;
     base.request.GET('https://jsplayground.000webhostapp.com/smsgateway/sendotp.php?mobile='+base.mobile+'&code='+base.teleCode).then(function(response){
         base.realcode = response;
     });
     this.system.showLoader("Sending verification code",3000,true);
  }

  verify(){
    let base = this;
    if(base.code.length != 4){
       this.system.showLoader("Invalid verification code",2000,false);
    }else{
       if(base.realcode == base.code){
          base.navCtrl.setRoot(SetupPage,{mobile:base.mobile});
       }
    }
  }

}
