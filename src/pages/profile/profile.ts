import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { Storage } from '@ionic/storage';
import { ModalPage } from '../modal/modal';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  name:string = "";
  profession:string = "";
  items: FirebaseListObservable<any[]>;
  avatar:string = "assets/img/avatar.png";
  number:string = "";
  status:string = "";

  constructor( public sqlite: SQLite,public modalCtrl: ModalController,public af: AngularFire, public navCtrl: NavController, public navParams: NavParams,public params: NavParams,public storage: Storage) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
    this.number = this.params.get('number');
    let base = this;
    base.showuser();
    base.updateFirebase();
  }

  showuser() {
     let base = this;
     let number = this.number;
        base.sqlite.create({
           name: 'localspry.db',
           location: 'default'
        }).then((db: SQLiteObject) => {
            let query = "select name,avatar,status,profession from contacts where number = '"+number+"'";
            db.executeSql(query, {}).then((data) =>{
                console.log(data.rows.item(0).name);
                    base.name = data.rows.item(0).name;
                    base.profession = data.rows.item(0).profession;
                    base.avatar = data.rows.item(0).avatar;
                    base.status = data.rows.item(0).status; 
            },error=>{
            console.log(error);
            });
        },error=>{
            console.log(error);
        });
   
  }

  updateFirebase() {
    let base = this;
    let number = base.number;
          let ref = base.af.database.object('/users/'+number);
          ref.subscribe(
              val => {
                let hasuser = val.$exists();
                if(hasuser){
                    base.status = val.status;
                    base.avatar = val.avatar;
                    base.profession = val.profession;


                         let avatar = val.avatar;
                         let status = val.status;
                         let petname = val.petname;
                         let profession = val.profession;
                         let lastupdate = val.lastupdate;
                         let location = val.location;
                         let display = "block";
                         let number = val.$key;

                    //update database local
                        base.sqlite.create({
                            name: 'localspry.db',
                            location: 'default'
                        }).then((db: SQLiteObject) => {
                            let query = "update contacts set petname = '"+petname+"',status= '"+status+"',avatar= '"+avatar+"',display= '"+display+"', lastupdate = '"+lastupdate+"', location = '"+location+"' where number = '"+number+"'";
                            db.executeSql(query, {}).then((success) =>{
                                console.log(success);                            
                            },error=>{
                                console.log(error);
                            });
                        },error=>{
                            console.log(error);
                        });

                }
              }
          );
  }

   showModal(src){
      let profileModal = this.modalCtrl.create(ModalPage, { photo: src });
      profileModal.present();
   }

}
