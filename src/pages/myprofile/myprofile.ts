import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalPage } from '../modal/modal';


@Component({
  selector: 'page-myprofile',
  templateUrl: 'myprofile.html'
})
export class MyprofilePage {

  name:string = "";
  profession:string = "";
  items: FirebaseListObservable<any[]>;
  avatar:string = "assets/img/avatar.png";
  number:string = "";
  status:string = "";
  location:string = "";
  lastupdate:string = "";
  contactlist:any = [];

    /* camera options */
  coptions: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetWidth:400,
        targetHeight:400
  }

  goptions: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth:400,
        targetHeight:400
  }

  constructor(public modalCtrl: ModalController, public alertCtrl: AlertController, public camera: Camera, public actionSheetCtrl: ActionSheetController, public af: AngularFire, public navCtrl: NavController, public navParams: NavParams,public params: NavParams,public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
        let base = this;

       base.storage.ready().then(() => {
           base.storage.get('currentuser').then((val) => {
               if(val != undefined){
                      let data = JSON.parse(val);
                      console.log(data);
                      this.name = data.name
                      this.profession = data.profession;
                      this.avatar = data.avatar;
                      this.number = data.number;
                      this.status = data.status;
                      this.location = data.location;
                      this.lastupdate = data.lastupdate;
               }
               base.updateFirebase();
            })
       });
  }

 uploadimagefromcamera(){
      let base = this;
        this.camera.getPicture(base.coptions).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
            base.avatar = base64Image;
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                avatar:base.avatar
            }; // data to be updated
            ref.update(fdata);

            let data = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.profession,
                        number: base.number
            }; // data to be pushed
            this.storage.set('currentuser',JSON.stringify(data));

        }, (err) => {
        });
  }

   uploadimagefromgallery(){
      let base = this;
        this.camera.getPicture(base.goptions).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
            base.avatar = base64Image;
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                avatar:base.avatar
            }; // data to be updated
            ref.update(fdata);

            let data = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.profession,
                        number: base.number
            }; // data to be pushed
            this.storage.set('currentuser',JSON.stringify(data));

        }, (err) => {
        });
  }

   deleteimage(){
      let base = this;
            base.avatar = "assets/img/avatar.png";
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                avatar:base.avatar
            }; // data to be updated
            ref.update(fdata);

            let data = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.profession,
                        number: base.number
            }; // data to be pushed
            this.storage.set('currentuser',JSON.stringify(data));
  }

    updateFirebase() {
    let base = this;
          let ref = base.af.database.object('/users/'+base.number);
          ref.subscribe(
              val => {
                let hasuser = val.$exists();
                if(hasuser){
                    base.status = val.status;
                    base.avatar = val.avatar;
                    base.profession = val.profession;
                    base.location = val.location;
                    base.lastupdate = val.lastupdate;
                    base.name = val.name;
                    let data = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : val.lastupdate,
                        location : val.profession,
                        number: base.number
                    }; // data to be pushed
                   this.storage.set('currentuser',JSON.stringify(data));
                }
              }
          );
  }

  //actnsheet controller
   presentActionSheet() {
   let base = this; 
   let actionSheet = this.actionSheetCtrl.create({
     title: 'profile picture action',
     buttons: [
       {
         text: 'Delete',
         icon:'trash',
         role: 'destructive',
         handler: () => {
           console.log('Destructive clicked');
           base.deleteimage();
         }
       },
       {
         text: 'Upload from camera',
         icon:'camera',
         handler: () => {
           console.log('Archive clicked');
           base.uploadimagefromcamera();
         }
       },
       {
         text: 'Upload from gallery',
         icon:'image',
         handler: () => {
           console.log('Archive clicked');
           base.uploadimagefromgallery();
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         icon:'hand',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
 }

 showNamePrompt() {
    let base = this;
    let prompt = this.alertCtrl.create({
      title: 'Change name',
      message: "Enter a new name",
      inputs: [
        {
          name: 'name',
          placeholder: base.name
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            base.name = data.name;
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                name:base.name
            }; // data to be updated
            ref.update(fdata);

                    let jdata = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.location,
                        number: base.number
                    }; // data to be pushed
                   this.storage.set('currentuser',JSON.stringify(jdata));
          }
        }
      ]
    });
    prompt.present();
 }

 showStatusPrompt() {
    let base = this;
    let prompt = this.alertCtrl.create({
      title: 'Change status',
      message: "Enter a new status",
      inputs: [
        {
          name: 'status',
          placeholder: base.status
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            base.status = data.status;
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                status:base.status
            }; // data to be updated
            ref.update(fdata);

                    let ndata = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.location,
                        number: base.number
                    }; // data to be pushed
            this.storage.set('currentuser',JSON.stringify(ndata));
          }
        }
      ]
    });
    prompt.present();
 }

 showProfessionPrompt() {
    let base = this;
    let prompt = this.alertCtrl.create({
      title: 'Change profession',
      message: "Enter a new profession",
      inputs: [
        {
          name: 'profession',
          placeholder: base.profession
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            base.profession = data.profession;
            let ref = base.af.database.object('/users/'+base.number);
            let fdata = {
                profession:base.profession
            }; // data to be updated
            ref.update(fdata);

                    let ndata = {
                        name : base.name,
                        avatar : base.avatar,
                        status : base.status,
                        profession : base.profession,
                        lastupdate : base.lastupdate,
                        location : base.location,
                        number: base.number
                    }; // data to be pushed
            this.storage.set('currentuser',JSON.stringify(ndata));
          }
        }
      ]
    });
    prompt.present();
 }

 showModal(src){
      let profileModal = this.modalCtrl.create(ModalPage, { photo: src });
      profileModal.present();
 }

}
