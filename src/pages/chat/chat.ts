import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {AngularFire, FirebaseListObservable} from 'angularfire2';  // firebase persistance
import { System } from '../../providers/system';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';


@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
  providers:[System]
})
export class ChatPage {
  
  messages:any = [];
  number : string;
  name: string;
  avatar:string;

  mobile:string;
  myavatar:string;
  message_db:string;
  message:string = "";

      /* camera options */
  coptions: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetWidth:400,
        targetHeight:400
  }

  goptions: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth:400,
        targetHeight:400
  }

  constructor( public actionSheetCtrl: ActionSheetController,public sqlite: SQLite,public system: System, public navCtrl: NavController, public navParams: NavParams,public storage: Storage, public af: AngularFire, public camera: Camera) {

   let base = this;
   base.number = base.navParams.get('number'); //contact's number
   
   //fetching contact's name and avatar
        base.sqlite.create({
           name: 'localspry.db',
           location: 'default'
        }).then((db: SQLiteObject) => {
            let query = "select name,avatar from contacts where number = '"+base.number+"'";
            db.executeSql(query, {}).then((data) =>{
                console.log(data.rows.item(0).name);
                    base.name = data.rows.item(0).name;
                    base.avatar = data.rows.item(0).avatar;
            },error=>{
            console.log(error);
            });
        },error=>{
            console.log(error);
        });


   base.storage.ready().then(() => {
           base.storage.get('currentuser').then((val) => {
               if(val != undefined){
                      let data = JSON.parse(val);
                      console.log(data);
                      base.mobile = data.number; //current user's number
                      base.myavatar = data.avatar;
                      if(parseInt(base.mobile) > parseInt(base.number)){
                          base.message_db = base.mobile+'-'+base.number;
                      }else{
                          base.message_db = base.number+'-'+base.mobile;
                      }
                      base.getMessages();
               }
          })
   });

   /** getting all chat from sqlite db **/
   
  }

  getMessages() {
    let base = this;
        base.sqlite.create({
              name: 'localspry.db',
              location: 'default'
        }).then((db: SQLiteObject) => {
            let query = "select * from chats where sender = '"+base.number+"' or receiver = '"+base.number+"'";
            db.executeSql(query, {}).then((data) =>{
                console.log(data.rows);
                for(var i=0;i<=data.rows.length-1;i++){
                    let currentData = data.rows.item(i);
                let content =  currentData.body;
                let time = base.unixTime(currentData.time);
                let type = currentData.type;
                let idisplay = '';
                let sdisplay = '';
                if(type == 'string'){
                   sdisplay = 'block';
                   idisplay = 'none';
                }else{
                  sdisplay = 'none';
                  idisplay = 'block';
                }
                
                let position = '';
                let senderName = '';
                let img = '';

                      if(currentData.sender == base.mobile){
                            let position = 'right';
                            let senderName = 'You';
                            let img = base.myavatar;
                                                        /** data to push **/
                            let pdata = {
                                  content :  content,
                                  position : position,
                                  time : time,
                                  sdisplay:sdisplay,
                                  idisplay:idisplay,
                                  senderName : senderName,
                                  img: img
                            }; 

                            base.messages.push(pdata);

                      }else{
                            
                            let position = 'left';
                            let senderName = base.name;
                            let img = base.avatar;  

                                                        /** data to push **/
                            let pdata = {
                                  content :  content,
                                  position : position,
                                  time : time,
                                  type:type,
                                  senderName : senderName,
                                  img: img
                            }; 

                            base.messages.push(pdata);
                      }  
                      base.scrolltoend();
                }

                base.getWholeChatFromFirebase();
                base.subscribeFirebase();

            },error=>{
              console.log(error);
            });
        },error=>{
            console.log(error);
        });
  
  }

  getWholeChatFromFirebase(){
       let base = this;
       let ref = base.af.database.list('/messages/'+base.message_db);
       ref.subscribe(val=>{
            console.log(val);
            /** for loop to filter the data/chat **/
            base.sqlite.create({
              name: 'localspry.db',
              location: 'default'
            }).then((db: SQLiteObject) => {

            for(var i=0;i<=val.length-1;i++){
                 let currentData = val[i];
                 let body = val[i].body;
                 let sender = val[i].sender;
                 let type = val[i].type;
                 let time = val[i].time;
                 let key = val[i].$key;
                 let receiver = "";
                 if(sender == base.number){
                    receiver = base.mobile;
                 }else{
                    receiver = base.number; 
                 }
                 let status = 'seen';

                /** check the existance of key in the database and then insert **/
                            let countquery = "select 'key' from chats where key = '"+key+"'";
                            
                            db.executeSql(countquery, {}).then(success=>{
                                console.log(success.rows.length);
                                 if(success.rows.length == 0){
                                        
                                        let query = "INSERT INTO chats (key,body,sender,receiver,type,time,status) VALUES ('"+key+"','"+body+"','"+sender+"','"+receiver+"','"+type+"','"+time+"','"+status+"')";
                                        db.executeSql(query, {}).then(success=>{
                                                                let content =  currentData.body;
                                                                let time = base.unixTime(currentData.time);
                                                                let type = currentData.type;
                                                                let idisplay = '';
                                                                let sdisplay = '';
                                                                if(type == 'string'){
                                                                  sdisplay = 'block';
                                                                  idisplay = 'none';
                                                                }else{
                                                                  sdisplay = 'none';
                                                                  idisplay = 'block';
                                                                }
                                                                
                                                                let position = '';
                                                                let senderName = '';
                                                                let img = '';

                                                                      if(currentData.sender == base.mobile){
                                                                            let position = 'right';
                                                                            let senderName = 'You';
                                                                            let img = base.myavatar;
                                                                                                        /** data to push **/
                                                                            let pdata = {
                                                                                  content :  content,
                                                                                  position : position,
                                                                                  time : time,
                                                                                  sdisplay:sdisplay,
                                                                                  idisplay:idisplay,
                                                                                  senderName : senderName,
                                                                                  img: img
                                                                            }; 

                                                                            base.messages.push(pdata);

                                                                      }else{
                                                                            
                                                                            let position = 'left';
                                                                            let senderName = base.name;
                                                                            let img = base.avatar;  

                                                                             /** data to push **/
                                                                            let pdata = {
                                                                                  content :  content,
                                                                                  position : position,
                                                                                  time : time,
                                                                                  type:type,
                                                                                  senderName : senderName,
                                                                                  img: img
                                                                            }; 

                                                                            base.messages.push(pdata);
                                                                      }  
                                        },error=>{
                                             console.log(error);
                                        });

                                 }
                            },error=>{
                                 console.log(error);
                            });

            }

            },error=>{
                console.log(error);
            });

       });
  }

  subscribeFirebase(){
       let base = this;
       let ref = base.af.database.list('/messages/'+base.message_db,{
           query: {
            orderByKey: true,
            limitToLast: 1,
          }
       });

        base.sqlite.create({
           name: 'localspry.db',
           location: 'default'
        }).then((db: SQLiteObject) => {
            
                   ref.subscribe(val=>{


                     console.log(val);
           
                 let body = val[0].body;
                 let sender = val[0].sender;
                 let type = val[0].type;
                 let time = val[0].time;
                 let key = val[0].$key;
                 let receiver = "";
                 if(sender == base.number){
                    receiver = base.mobile;
                 }else{
                    receiver = base.number; 
                 }
                 let status = 'seen';

                /** check the existance of key in the database and then insert **/
                            let countquery = "select 'key' from chats where key = '"+key+"'";
                            
                            db.executeSql(countquery, {}).then(success=>{
                                console.log(success.rows.length);
                                 if(success.rows.length == 0){
                                        
                                        let query = "INSERT INTO chats (key,body,sender,receiver,type,time,status) VALUES ('"+key+"','"+body+"','"+sender+"','"+receiver+"','"+type+"','"+time+"','"+status+"')";
                                        db.executeSql(query, {}).then(success=>{
                                                                let currentData = val[0];
                                                                let content =  currentData.body;
                                                                let time = base.unixTime(currentData.time);
                                                                let type = currentData.type;
                                                                let idisplay = '';
                                                                let sdisplay = '';
                                                                if(type == 'string'){
                                                                  sdisplay = 'block';
                                                                  idisplay = 'none';
                                                                }else{
                                                                  sdisplay = 'none';
                                                                  idisplay = 'block';
                                                                }
                                                                
                                                                let position = '';
                                                                let senderName = '';
                                                                let img = '';

                                                                      if(currentData.sender == base.mobile){
                                                                            let position = 'right';
                                                                            let senderName = 'You';
                                                                            let img = base.myavatar;
                                                                                                        /** data to push **/
                                                                            let pdata = {
                                                                                  content :  content,
                                                                                  position : position,
                                                                                  time : time,
                                                                                  sdisplay:sdisplay,
                                                                                  idisplay:idisplay,
                                                                                  senderName : senderName,
                                                                                  img: img,
                                                                                  type:type
                                                                            }; 

                                                                            base.messages.push(pdata);

                                                                      }else{
                                                                            
                                                                            let position = 'left';
                                                                            let senderName = base.name;
                                                                            let img = base.avatar;  

                                                                             /** data to push **/
                                                                            let pdata = {
                                                                                  content :  content,
                                                                                  position : position,
                                                                                  time : time,
                                                                                  sdisplay:sdisplay,
                                                                                  idisplay:idisplay,
                                                                                  senderName : senderName,
                                                                                  img: img,
                                                                                  type:type
                                                                            }; 

                                                                            base.messages.push(pdata);
                                                                      }  
                                        },error=>{
                                             console.log(error);
                                        });

                                 }
                            },error=>{
                                 console.log(error);
                            });

       });

        },error=>{
            console.log(error);
        });
  }

  sendmessage(){
    let base = this;
    let message = base.message;
    base.message = "";
    if(message == ""){
        base.presentActionSheet();
    }else{
      let ref = base.af.database.list('/messages/'+base.message_db);
      let d = new Date();
      let time = d.getTime();
      let mdata = {
        body:message,
        sender:base.mobile,
        type:'string',
        time:time
      };
      ref.push(mdata);
    }
  }


unixTime(time) {

      let d = new Date();
      let currentTime = d.getTime().toString();
      var currentDiff = parseInt(currentTime)-parseInt(time);
      var secDiff = currentDiff/1000;
      if(secDiff <= 60 ){
          return 'few sec ago';
      }else if(secDiff <= 1800){
        return 'rew min ago';
      }else if(secDiff > 1800 && secDiff < 3600){
        return 'half hour ago';
      }else if(secDiff >3600 && secDiff < 24*3600){
          return parseInt((secDiff/3600).toString().split(',')[0])+'hours ago';
      }else{
      
      var u = new Date(time);
      return u.getUTCFullYear() +
        '/' + ('0' + u.getUTCMonth()).slice(-2) +
        '/' + ('0' + u.getUTCDate()).slice(-2) + 
        ' , ' + ('0' + u.getUTCHours()).slice(-2) +
        ':' + ('0' + u.getUTCMinutes()).slice(-2);
      }

}

scrolltoend(){
   document.getElementById('chats').scrollTop = document.getElementById('chats').scrollHeight;
}

//actnsheet controller
   presentActionSheet() {
   let base = this; 
   let actionSheet = this.actionSheetCtrl.create({
     title: 'send image',
     buttons: [
       {
         text: 'Upload from camera',
         icon:'camera',
         handler: () => {
           console.log('Archive clicked');
           base.uploadimagefromcamera();
         }
       },
       {
         text: 'Upload from gallery',
         icon:'image',
         handler: () => {
           console.log('Archive clicked');
           base.uploadimagefromgallery();
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         icon:'hand',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
 }

  uploadimagefromcamera(){
      let base = this;
        this.camera.getPicture(base.coptions).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
            let ref = base.af.database.list('/messages/'+base.message_db);
            let d = new Date();
            let time = d.getTime();
            let mdata = {
              body:base64Image,
              sender:base.mobile,
              type:'image',
              time:time
            };
            ref.push(mdata);

        }, (err) => {
        });
  }

   uploadimagefromgallery(){
      let base = this;
        this.camera.getPicture(base.goptions).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
            let ref = base.af.database.list('/messages/'+base.message_db);
            let d = new Date();
            let time = d.getTime();
            let mdata = {
              body:base64Image,
              sender:base.mobile,
              type:'image',
              time:time
            };
            ref.push(mdata);

        }, (err) => {
        });
  }

}
