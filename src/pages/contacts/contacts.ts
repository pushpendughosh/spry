import { Component, OnInit, OnDestroy } from '@angular/core';
import {App, IonicApp, NavController, NavParams,Platform  } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { ChatPage } from '../chat/chat';
import { Storage } from '@ionic/storage';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../popover/popover';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";


@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html'
})
export class ContactsPage {

  items: FirebaseListObservable<any[]>;
  mobile:string = "9734072595";
  contactlist:any = [];
  contactcount:string = "0";
  time:number = 0;

//tymer
  private tick: string;
  private subscription: Subscription;


  constructor(public sqlite: SQLite, public popoverCtrl: PopoverController,public af: AngularFire,private app: App , public navCtrl: NavController, public navParams: NavParams,public storage: Storage, public contacts: Contacts,public platform: Platform ) {
    let base = this;
    base.storage.set('verified',true);
  }

  showusers(){
    
    let base = this;
    base.sqlite.create({
           name: 'localspry.db',
           location: 'default'
    }).then((db: SQLiteObject) => {
        let query = "select name,status,avatar,display,number from contacts";
         db.executeSql(query, {}).then((data) =>{
             base.showcontacts(data.rows);
             base.subscribeFirebase(data.rows);
         },error=>{
           console.log(error);
         });
    },error=>{
        console.log(error);
    });

  }

  activeusers(){
          let base = this;
    base.sqlite.create({
           name: 'localspry.db',
           location: 'default'
    }).then((db: SQLiteObject) => {
        let query = "select name,number from contacts where display = 'block'";
         db.executeSql(query, {}).then((data) =>{
             base.updateFirebaseContacts(data.rows);
         },error=>{
           console.log(error);
         });
    },error=>{
        console.log(error);
    });
  }

  searchusers(){

      let base = this;
        this.platform.ready().then(() => {
              //contacts
              let options = {   
                   filter : "",                                
                   multiple: true,        
                   hasPhoneNumber:true,                             
                   fields:  [ 'displayName', 'name' ]
              };


                    this.sqlite.create({
                        name: 'localspry.db',
                        location: 'default'
                    }).then((db: SQLiteObject) => {

              base.contacts.find([ 'displayName', 'name' ],options).then((contacts) => {

                  console.log(contacts);
                  //on contact success
                  for(var a=0;a<=contacts.length-1;a++){
                        let number = contacts[a].phoneNumbers[0].value.replace('-','').replace(')','').replace('(','').replace('+','').replace(' ','').replace('  ','').trim().slice(-10);
                        let name = contacts[a].displayName;
                        let avatar = "assets/img/avatar.png";
                        let status = "Hey ! i am using spry";
                        let petname = "pet name";
                        let profession = "spry";
                        let lastupdate = "0";
                        let location = "0";
                        let display = "none";
                        
                        //start statement
                        if( number != base.mobile ){
                            console.log(number);
                            let countquery = "select 'name' from contacts where number= '"+number+"'";
                            
                            db.executeSql(countquery, {}).then(success=>{
                                console.log(success.rows.length);
                                 if(success.rows.length == 0){
                                        
                                        let query = "INSERT INTO contacts (name,number,avatar,profession,petname,status,lastupdate,location,display) VALUES ('"+name+"','"+number+"','"+avatar+"','"+profession+"','"+petname+"','"+status+"','"+lastupdate+"','"+location+"','"+display+"')";
                                        db.executeSql(query, {}).then(success=>{
                                             console.log(success);
                                        },error=>{
                                             console.log(error);
                                        });

                                 }
                            },error=>{
                                 console.log(error);
                            });

                        }
                        //end statement

                  }

              }, (error) => {
                   console.log(error);
              });
            //end contacts

                    },error=>{
                        console.log(error);
                    });
        });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
      let base = this;
      base.contactlist = [];
        this.storage.get('mobile').then(function(mobile){
           base.mobile = mobile;
        });
        
        base.showusers(); //show all user

        let timer = TimerObservable.create(2000, 10000);
        base.subscription = timer.subscribe(t => {
             base.searchusers(); //search for new added contacts
             base.activeusers();
        });
  }

  animateSearch(){
     let title = document.getElementById("toolbar-title");
     let searchicon = document.getElementById("search-icon");
     let searchbar = document.getElementById("searchbar");
     title.style.display = 'none';
     searchicon.style.display = 'none';
     searchbar.style.display = 'block';
  }

  hideSearch(){
     let title = document.getElementById("toolbar-title-md");
     let searchicon = document.getElementById("search-icon");
     let searchbar = document.getElementById("searchbar");
     title.style.display = 'block';
     searchicon.style.display = 'block';
     searchbar.style.display = 'none';
  }

  showProfile(number){
     let data = {
         number:number
     };
    //this.navCtrl.push(ProfilePage,data);
    this.app.getRootNav().push(ProfilePage,data);
  }

  showcontacts(contacts){
     let base = this;
     for(var a=0;a<=contacts.length-1;a++){
         let name = contacts.item(a).name;
         let avatar = contacts.item(a).avatar;
         let status = contacts.item(a).status;
         let display = contacts.item(a).display;
         let number = contacts.item(a).number;
         //start statement
              base.contactlist.push({
                name:name,
                avatar:avatar,
                status:status,
                display:display,
                number:number
              });
         //end statement
     }
  }

  subscribeFirebase(contacts){
     let base = this;
     for(var a=0;a<=contacts.length-1;a++){
         let number = contacts.item(a).number;
         //start statement
          let ref = base.af.database.object('/users/'+number);
          ref.subscribe(
              val => {
                let hasuser = val.$exists();
                if(hasuser){
                    let index = base.getindex(val.$key,base.contactlist);
                    base.contactlist[index].status = val.status;
                    base.contactlist[index].avatar = val.avatar;
                    base.contactlist[index].display = 'block';
                    
                         let avatar = val.avatar;
                         let status = val.status;
                         let petname = val.petname;
                         let profession = val.profession;
                         let lastupdate = val.lastupdate;
                         let location = val.location;
                         let display = "block";
                         let number = val.$key;

                    //update database local
                        base.sqlite.create({
                            name: 'localspry.db',
                            location: 'default'
                        }).then((db: SQLiteObject) => {
                            let query = "update contacts set petname = '"+petname+"',status= '"+status+"',avatar= '"+avatar+"',display= '"+display+"', lastupdate = '"+lastupdate+"', location = '"+location+"' where number = '"+number+"'";
                            db.executeSql(query, {}).then((success) =>{
                                //console.log(success);                            
                            },error=>{
                                console.log(error);
                            });
                        },error=>{
                            console.log(error);
                        });

                }else{

                        base.sqlite.create({
                            name: 'localspry.db',
                            location: 'default'
                        }).then((db: SQLiteObject) => {
                            let query = "update contacts set display= 'none' where number = '"+number+"'";
                            db.executeSql(query, {}).then((success) =>{
                                //console.log(success);  
                                                    let index = base.getindex(val.$key,base.contactlist);
                                                    base.contactlist[index].display = 'none';                          
                            },error=>{
                                console.log(error);
                            });
                        },error=>{
                            console.log(error);
                        });

                }
              }
          );
     }
  }

  updateFirebaseContacts(contacts){
     let base = this;
     let activeusers = [];
     let ref = base.af.database.object('/users/'+base.mobile+'/contacts');
     ref.set('').then(function(){
            for(var a=0;a<=contacts.length-1;a++){
                let number = contacts.item(a).number;
                let name = contacts.item(a).name;
                activeusers.push({
                    name:name,
                    number:number
                });
                if(a == contacts.length-1){
                     ref.set(activeusers);
                }
            }
     });
  }

  getindex(string,array){
    let b = 0;
     for(b=0;b<=array.length-1;b++){
         let number = array[b].number;
         if(number == string){
             return b;
         }else{
             if(b == array.length-1){
                 return -1;
             }
         }
     }
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  showchat(number){
     let data = {
         number:number
     };
    this.app.getRootNav().push(ChatPage,data);
  }

}
