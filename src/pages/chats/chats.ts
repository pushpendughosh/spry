import { Component, OnInit, OnDestroy } from '@angular/core';
import {App, IonicApp, NavController, NavParams,Platform  } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { Storage } from '@ionic/storage';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { PopoverController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";


@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html'
})
export class ChatsPage {

  items: FirebaseListObservable<any[]>;
  mobile:string = "9734072595";
  contactlist:any = [];
  contactcount:string = "0";
  time:number = 0;

//tymer
  private tick: string;
  private subscription: Subscription;


  constructor(public sqlite: SQLite, public popoverCtrl: PopoverController,public af: AngularFire,private app: App , public navCtrl: NavController, public navParams: NavParams,public storage: Storage, public contacts: Contacts,public platform: Platform ) {

  }

  showchat(number){
     let data = {
         number:number
     };
    this.app.getRootNav().push(ChatPage,data);
  }


}
