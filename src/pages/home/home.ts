import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { IntroPage } from '../intro/intro';
import { System } from '../../providers/system';
import { Request } from '../../providers/request';
import { OtpPage } from '../otp/otp';
import { AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[System,Request]
})
export class HomePage {
  mobile:string = "";
  teleCode:string = "";

  constructor(public params: NavParams,public request: Request , public navCtrl: NavController,public system: System,public alertCtrl: AlertController, private geolocation: Geolocation) {
  }

  sendoOTP(){
     let base = this;
     if(base.mobile.length <= 10){
        base.system.showLoader("Incorrect mobile number",2000,false);
     }else{

          let confirm = this.alertCtrl.create({
            title: base.mobile,
            message: 'A verification code will be sent to this number via text message',
            buttons: [
              {
                text: 'Cancel'
              },
              {
                text: 'OK',
                handler: () => {
                  let phone = base.mobile.slice(-10);
                  let code =  base.mobile.replace(base.mobile.slice(-10),'');
                  base.request.GET('https://jsplayground.000webhostapp.com/smsgateway/sendotp.php?mobile='+phone+'&code='+code).then(function(response){
                      console.log(response);
                      base.system.showLoader('you otp is'+response,5000,false);
                      base.navCtrl.setRoot(OtpPage,{
                        otp:response,
                        mobile:phone,
                        teleCode:code
                      });
                  });
                  
                }
              }
            ]
          });
          confirm.present()
          
     }
  }

  ionViewDidLoad() {
      console.log(this.system.isNetworkAvailable());
  }


}
