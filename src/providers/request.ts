import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';

@Injectable()
export class Request {
  apibaseurl = 'http://localhost/50630/api/public/api';
  constructor(public http: Http,public toastCtrl: ToastController) {
    console.log('Hello Request Provider');
  }
  public POST(url:string , data:any, token:any=null){
    let base = this;
    return new Promise(function(resolve, reject) {
      let headers = new Headers();
      headers.append('content-type', 'application/json');
      if(token!=null){
        headers.append('Authorization', 'Bearer '+ token);
      }
      let options = new RequestOptions({ headers: headers });
      base.http.post(url, data, options)
          .map(res => res.json())
          .subscribe(function (success) {
            resolve(success);
          }, function (error) {
            reject(error);
          });
    });
  }
  public NOFITY(message:any){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
    
  }
  public GET(url:string){
    let base = this;
    return new Promise(function(resolve, reject) {
      let headers = new Headers();
      headers.append('content-type', 'application/json');
      let options = new RequestOptions({ headers: headers });
      base.http.get(url)
          .map(res => res.json())
          .subscribe(function (success) {
            resolve(success);
          }, function (error) {
            reject(error);
          });
    });
  }



}