import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';


@Injectable()
export class System {

  constructor(public network: Network, private platform: Platform, private diagnostic: Diagnostic,public loadingCtrl: LoadingController) {
    console.log('Hello System Provider');
  }

  /** check location status **/
  public checkLocationStatus(){
    let base = this;
    this.diagnostic.isLocationEnabled().then(successData=>{
      console.log("Location service enabled");
    }).catch(errorData=>{
      base.diagnostic.switchToLocationSettings();
      // let loader = this.loadingCtrl.create({
      //   content: "Please enable location service...",
      //   duration: 2000
      // });
      // loader.present();
      // base.platform.exitApp();
    });
  }  

  public showLoader(message:string,duration:number,spinner:boolean){
    if(spinner == null || spinner == false){
      let loader = this.loadingCtrl.create({
        content: message,
        duration: duration,
        spinner:'hide'
      });
      loader.present(); 
    }else{
      let loader = this.loadingCtrl.create({
        content: message,
        duration: duration
      });
      loader.present();
    }
      
  }

  /** check internet / network availability **/
  public isNetworkAvailable(){
      let success = true;
      let error = false;
      let base = this;

    return new Promise(function(resolve, reject) {
      let network = base.network.type;
          if(network === 'unknown' || network === 'none'){
            resolve(success);
          }else{
            reject(error);
          }
    });

  }

  
}
