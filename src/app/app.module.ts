import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { IntroPage } from '../pages/intro/intro';
import { OtpPage } from '../pages/otp/otp';
import { SetupPage } from '../pages/setup/setup';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ContactsPage } from '../pages/contacts/contacts';
import { TimelinePage } from '../pages/timeline/timeline';
import { ProfilePage } from '../pages/profile/profile';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { PopoverPage } from '../pages/popover/popover';
import { ModalPage } from '../pages/modal/modal';
import { ChatsPage } from '../pages/chats/chats';
import { ChatPage } from '../pages/chat/chat';
import { Geolocation } from '@ionic-native/geolocation';
import { AngularFireModule } from 'angularfire2';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { AddgigPage } from '../pages/addgig/addgig';
import { MygigsPage } from '../pages/mygigs/mygigs';



// Must export the config
export const firebaseConfig = {
    apiKey: "AIzaSyBibNqakl3NHpSqQt7IOPp-dpisC_1zcMc",
    authDomain: "spry-ec52a.firebaseapp.com",
    databaseURL: "https://spry-ec52a.firebaseio.com",
    projectId: "spry-ec52a",
    storageBucket: "spry-ec52a.appspot.com",
    messagingSenderId: "25938758058"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IntroPage,
    OtpPage,
    SetupPage,
    DashboardPage,
    ContactsPage,
    ProfilePage,
    MyprofilePage,
    PopoverPage,
    ModalPage,
    TimelinePage,
    ChatsPage,
    ChatPage,
    AddgigPage,
    MygigsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    IntroPage,
    OtpPage,
    SetupPage,
    DashboardPage,
    ContactsPage,
    ProfilePage,
    MyprofilePage,
    PopoverPage,
    ModalPage,
    TimelinePage,
    ChatsPage,
    ChatPage,
    AddgigPage,
    MygigsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Diagnostic,
    Geolocation,
    Camera,
    Contacts,
    SQLite,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
