import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { IntroPage } from '../pages/intro/intro';
import {System} from '../providers/system';
import { SetupPage } from '../pages/setup/setup';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ProfilePage } from '../pages/profile/profile';
import {MyprofilePage } from '../pages/myprofile/myprofile';
import {OtpPage } from '../pages/otp/otp';
import { TimelinePage } from '../pages/timeline/timeline';
import { ChatsPage } from '../pages/chats/chats';
import { ChatPage } from '../pages/chat/chat';
import { Network } from '@ionic-native/network';
import { AddgigPage } from '../pages/addgig/addgig';
import { MygigsPage } from '../pages/mygigs/mygigs';



@Component({
  templateUrl: 'app.html',
  providers: [System]
})
export class MyApp {
  rootPage:any = "";

  constructor(platform: Platform,public statusBar: StatusBar, splashScreen: SplashScreen,public storage: Storage, public system: System, public network: Network) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // let status bar overlay webview
      //this.statusBar.overlaysWebView(true);
      // set status bar to white
      this.statusBar.hide();
      splashScreen.hide();
      this.system.checkLocationStatus();
      this.showSliders();

    });
  }

    showSliders(){
    let base = this;
      base.storage.ready().then(() => {
             base.storage.get('verified').then((val) => {
                   if(!val){
                      base.storage.get('intro-viewed').then((val) => {
                        console.log(val);
                          if(!val){
                                base.rootPage = IntroPage;
                                base.storage.set('intro-viewed',true); 
                          }else{
                                base.rootPage = HomePage;
                          }
                      })
                   }else{
                        base.rootPage = DashboardPage;
                   }
              })
     });
  }

}
